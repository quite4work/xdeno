// https://letsencrypt.org/docs/staging-environment/#root-certificates
export async function stagingCaCerts() {
  const x1 = await (await fetch(
    "https://letsencrypt.org/certs/staging/letsencrypt-stg-root-x1.pem",
  )).text();
  const x2 = await (await fetch(
    "https://letsencrypt.org/certs/staging/letsencrypt-stg-root-x2.pem",
  )).text();
  return [x1, x2];
}
