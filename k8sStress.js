import * as deno from "./deno.js";
import * as k8s from "./k8s.js";
import * as kubectl from "./kubectl.js";

const cron = `
import { serve } from "https://deno.land/std@0.148.0/http/server.ts";
console.log("start");

const server = serve({ port: 80 });

const ping = async () => {
  console.log("ping");
  const res = await fetch("http://cron");
  const text = await res.text();
  console.log(text);
}

const job = ping();

for await (const request of server) {
  request.respond({ status: 200, body: "pong"  });
  await job;
  break;
}
console.log("stop");
Deno.exit(0);
`;

export function k8sStressYargsCommands(opts) {
  return [{
    command: "crons deploy",
    desc: "Deploy crons",
    builder: {
      count: {
        alias: "c",
        type: "number",
        default: 3,
        description: "Cron count",
      },
      suspend: {
        alias: "s",
        type: "boolean",
        default: false,
        description: "Suspend crons",
      },
      namespace: {
        alias: "n",
        type: "string",
        description: "Deploy to namespace",
      },
    },
    handler: (argv) => cronsDeploy(argv.count, { ...opts, ...argv }),
  }, {
    command: "crons clean",
    desc: "Clean crons",
    builder: {
      namespace: {
        alias: "n",
        type: "string",
        description: "Deploy to namespace",
      },
    },
    handler: (argv) => cronsClean({ ...opts, ...argv }),
  }];
}

export function setup(defaultOpts) {
  return {
    cronsClean: (opts) => cronsClean(opts, { ...defaultOpts, ...opts }),
    cronsDeploy: (count, opts) =>
      cronsDeploy(count, { ...defaultOpts, ...opts }),
    cronsDeployed: (opts) => cronsDeployed({ ...defaultOpts, ...opts }),
  };
}

export async function cronsClean(opts) {
  await kubectl.kubectl(`delete cronjob -l "test-cron=true"`, opts);
}

export async function cronsDeployed(opts) {
  return await kubectl.isExists("cj", "cron-0", opts);
}

export async function cronsDeploy(count, { suspend, namespace, context }) {
  const crons = [];
  const src = await deno.bundle(cron);
  for (let n = 0; n < count; n++) {
    crons.push(
      await denoCron("cron-" + n, src, {
        suspend,
        concurrencyPolicy: "Replace",
      }),
    );
  }
  crons.push(
    k8s.service("cron", "cron"),
  );
  await kubectl.createNamespace(namespace, { context });
  const cronsFlat = crons.flat(Infinity);
  const chunks = _.chunk(cronsFlat, 100);
  let n = cronsFlat.length;
  console.log(`remained: ${n}`);
  const jobs = [];
  for (const chunk of chunks) {
    // console.dir(chunk, { depth: Infinity });
    jobs.push((async () => {
      await kubectl.apply(chunk, {
        context,
        namespace,
      });
      n -= chunk.length;
      console.log(`remained: ${n}`);
    })());
  }
  await Promise.all(jobs);
}

async function denoCron(
  name,
  src,
  {
    schedule = "* * * * *",
    suspend = false,
    port = 80,
    concurrencyPolicy = "Forbid",
  } = {},
) {
  const command = [
    "sh",
    "-c",
    `deno run --unstable -A /conf/main.js`,
  ];
  const denoConfig = k8s.configMap(name, { "main.js": src });
  const denoCronJob = k8s.cronJob(
    name,
    "denoland/deno",
    command,
    { suspend, schedule, port, config: name, concurrencyPolicy },
  );
  return [denoConfig, denoCronJob];
}
