export function getKey(object, pred) {
  return Object.keys(object).find((key) => pred(object[key]));
}
