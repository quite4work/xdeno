import * as server from "https://deno.land/std@0.148.0/http/server.ts";

export async function handler(_req, { interval, block }) {
  let timer;
  if (block) {
    const body = new ReadableStream({
      async start(controller) {
        timer = setInterval(() => {
          controller.enqueue(new TextEncoder().encode(".\n"));
        }, interval);
      },
      cancel() {
        clearInterval(timer);
      },
    });
    return new Response(body, {
      headers: {
        "content-type": "text/plain; charset=utf-8",
        "transfer-encoding": "chunked",
      },
    });
  } else {
    return new Response(".");
  }
}

export async function serve({ port = 80, interval = 1000, block }) {
  await server.serve((req) => handler(req, { interval, block }), { port });
}
