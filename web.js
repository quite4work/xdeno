import html from "https://gitlab.com/xdeno/utils/-/raw/0.0.1/literal.js";
import { link } from "./proc.js";
import { getFreePort } from "https://deno.land/x/free_port@v1.2.0/mod.ts";

export function index(js, opts) {
  return html`
<html>
  <script type="module">
    ${js}
    render(${JSON.stringify(opts)});
  </script>
  <body>
    <div id="app"></div>
  </body>
</html>
`;
}

// TODO: stop using temp file when inline variant becomes faster
//      `data:application/javascript,${encodeURIComponent(index(js, opts))}`
export async function start(run, opts0) {
  const { preferredPort = 8080 } = opts0;
  const port = await getFreePort(preferredPort, { hostname: "localhost" });
  const webview = import.meta.resolve("./webview.js");
  const compiler = import.meta.resolve("./compiler.js");
  const logWebFront = import.meta.resolve("./logWebFront.jsx");
  const tmp = await Deno.makeTempFile({ suffix: ".html" });
  try {
    const opts = { ...opts0, port };
    const src = [
      `import { index } from "${import.meta.url}";`,
      `import { open } from "${webview}";`,
      `import { bundleWeb } from "${compiler}";`,
      `const js = await bundleWeb("${logWebFront}");`,
      `const html = index(js, ${JSON.stringify(opts)});`,
      `await Deno.writeTextFile("${tmp}", html);`,
      `open("file://${tmp}");`,
    ].join(" ");
    const { pid } = await Deno.spawnChild(Deno.execPath(), {
      args: ["eval", "--unstable", src],
      stdout: "inherit",
      stderr: "inherit",
    });
    link(pid);
    await run(opts);
  } finally {
    await Deno.remove(tmp);
  }
}
