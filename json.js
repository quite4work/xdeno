import { isPlainObject } from "https://cdn.skypack.dev/lodash-es@4.17.21";

export function traverse(term, callback) {
  if (Array.isArray(term)) {
    for (const item of term) {
      traverse(item, callback);
    }
  } else if (isPlainObject(term)) {
    for (const [k, v] of Object.entries(term)) {
      callback(k, v);
      traverse(v, callback);
    }
  }
}
