import * as web from "./web.js";
import * as ktail from "./ktail.js";
import * as log from "./log.js";
import * as ws from "./ws.js";
import * as object from "./object.js";

export async function start(opts = {}) {
  await web.start(run, opts);
}

const cols = [
  "level",
  "inferredLevel",
  "host",
  "pod",
  "ts",
  "msg",
];

function run(opts) {
  const logs = processLogs(ktail.ktail(opts));
  async function onopen(ws) {
    for await (const log of logs) {
      ws.send(JSON.stringify(log));
    }
  }
  return new Promise((resolve) => {
    ws.start({
      ...opts,
      onopen: async (ws) => {
        resolve();
        await onopen(ws);
      },
    });
  });
}

async function* processLogs(logs) {
  let lastMsgTime = performance.now();
  const msg =
    "================================================================================";
  for await (const l of logs) {
    if (performance.now() - lastMsgTime > 60000) {
      yield* handleLog({
        pod: "",
        host: "",
        namespace: "",
        event: "logMsg",
        msg,
        ts: new Date(),
      });
    }
    lastMsgTime = performance.now();
    yield* handleLog(l);
  }
}

async function* handleLog(l) {
  switch (l.type) {
    case "logMsg":
      yield formatLogMsg(l);
      break;
    case "error":
    case "logEnd":
    case "logStart":
    case "podDelete":
    case "podModify":
    case "podAdd":
    default:
      yield formatOther(l);
  }
}

function formatOther(l) {
  const { pod, host, ts, type } = l;
  return {
    ts,
    pod,
    host,
    msg: `========================= EVENT: ${type} =========================`,
    cols,
    level: "",
    inferredLevel: "",
  };
}

function formatLogMsg(l) {
  const { pod, host, msg, ts } = l;
  const inferredLevel = log.inferLevel(msg, "");
  let level = "";
  const parsed = log.parse2(msg);
  if (parsed) {
    const levelKey = object.getKey(parsed, log.parseLevel);
    level = log.parseLevel(parsed[levelKey]) ?? "";
  }
  return {
    ts,
    pod,
    host,
    msg,
    level,
    inferredLevel,
    cols,
  };
}
