import { decode, encode } from "https://deno.land/std@0.148.0/encoding/hex.ts";

export default class Storage {
  constructor(name, context) {
    return new Proxy(this, new StorageProxy(name, context));
  }
}

// https://deno.land/manual/runtime/web_storage_api#web-storage-api
// If there is no configuration or location specifier, Deno uses the absolute
// path to the main module to determine what storage is shared. (Deno.mainModule)
class StorageProxy {
  #context;
  #cache = {};

  constructor(name) {
    this.#context = import.meta.url + name;
    for (const key of keys(this.#context)) {
      this.#cache[key] = get(this.#context, key);
    }
  }

  get(_, key) {
    if (typeof key === "symbol") {
      return;
    }
    if (!(key in this.#cache)) {
      this.#cache[key] = get(this.#context, key);
    }
    return this.#cache[key];
  }

  set(_, key, val) {
    set(this.#context, key, val);
    this.#cache[key] = val;
    return true;
  }

  deleteProperty(_, key) {
    remove(this.#context, key);
    delete this.#cache[key];
    return true;
  }

  has(_, key) {
    if (typeof key === "symbol") {
      return false;
    }
    if (!(key in this.#cache)) {
      this.#cache[key] = get(this.#context, key);
    }
    return this.#cache[key] !== undefined;
  }

  ownKeys(_) {
    return Object.keys(this.#cache);
  }

  // https://stackoverflow.com/a/40352698
  getOwnPropertyDescriptor(_) {
    return {
      enumerable: true,
      configurable: true,
    };
  }
}

function keys(context) {
  return Object.keys(localStorage)
    .filter((k) =>
      k.startsWith(
        new TextDecoder().decode(encode(new TextEncoder().encode(context))) +
          "_",
      )
    )
    .map((k) =>
      new TextDecoder().decode(decode(new TextEncoder().encode(
        k.replace(
          new TextDecoder().decode(encode(new TextEncoder().encode(context))) +
            "_",
          "",
        ),
      )))
    );
}

function get(context, key) {
  const data = localStorage.getItem(
    new TextDecoder().decode(encode(new TextEncoder().encode(context))) + "_" +
      new TextDecoder().decode(encode(new TextEncoder().encode(key))),
  );
  if (data) {
    return JSON.parse(data);
  }
}

function remove(context, key) {
  localStorage.removeItem(
    new TextDecoder().decode(encode(new TextEncoder().encode(context))) + "_" +
      new TextDecoder().decode(encode(new TextEncoder().encode(key))),
  );
}

function set(context, key, val) {
  if (!val && val !== false) {
    // Do not save empty values unless it is boolean `false`
    return;
  }
  localStorage.setItem(
    new TextDecoder().decode(encode(new TextEncoder().encode(context))) + "_" +
      new TextDecoder().decode(encode(new TextEncoder().encode(key))),
    JSON.stringify(val),
  );
}
