import { parse, stringify } from "./yaml.js";

export async function read(file, opts) {
  let schema;
  if (opts) {
    schema = opts.schema;
  }
  const yml = await Deno.readTextFile(file);
  return parse(yml, { schema });
}

export async function write(file, obj, opts) {
  let schema;
  if (opts) {
    schema = opts.schema;
  }
  const yml = stringify(obj, { schema });
  await Deno.writeTextFile(file, yml);
}

export async function edit(file, replace, opts) {
  let yml = await read(file, opts);
  if (replace instanceof Function) {
    const res = await replace(yml, opts);
    if (res) {
      yml = res;
    }
  } else {
    yml = { ...yml, ...replace };
  }
  await write(file, yml, opts);
}

export function configure(defaultOpts) {
  return {
    read(file, opts) {
      read(file, { ...opts, ...defaultOpts });
    },
    write(file, obj, opts) {
      write(file, obj, { ...opts, ...defaultOpts });
    },
    edit(file, func, opts) {
      edit(file, func, { ...opts, ...defaultOpts });
    },
  };
}
