import err from "./err.js";
import { paramCase } from "https://deno.land/x/case@2.1.1/mod.ts";

// TODO: refactor

async function records(
  domainName,
  { authId, authPassword, verbosity, type },
) {
  const res = await api("/dns/records.json", {
    domainName,
    authId,
    authPassword,
    type,
  }, {
    verbosity,
  });
  return Object.values(res);
}

async function updateHost(
  host,
  domainName,
  ip,
  { authId, authPassword, verbosity, ttl = 60 },
) {
  const rec = await records(domainName, {
    authId,
    authPassword,
    verbosity,
    type: "A",
  });
  const [{ id: recordId }] = rec.filter(({ host: x }) => x === host);
  await updateRecord(domainName, {
    host,
    record: ip,
    recordId,
    authId,
    authPassword,
    verbosity,
    ttl,
  });
}

async function updateRecord(
  domainName,
  { host = "", recordId, authId, authPassword, record, verbosity, ttl = 60 },
) {
  await api("/dns/mod-record.json", {
    authId,
    authPassword,
    domainName,
    recordId,
    host,
    record,
    ttl,
  }, {
    verbosity,
  });
}

async function api(path, qs, { verbosity }) {
  const qs2 = {};
  for (const [k, v] of Object.entries(qs)) {
    qs2[paramCase(k)] = v;
  }
  const url = "https://api.cloudns.net" + path + "?" + new URLSearchParams(qs2);
  if (verbosity > 1) {
    console.error(url);
  }
  const res = await fetch(url, { method: "GET" });
  if (verbosity) {
    console.error(url);
  }
  if (!res.ok) {
    throw err("request failed", { url, resp });
  }
  const resp = await res.json();
  if (verbosity) {
    console.error(resp);
  }
  return resp;
}

function setup(defaultOpts = {}) {
  const setup2 = (opts = {}) => setup({ ...defaultOpts, ...opts });
  setup2.updateRecord = (domain, opts = {}) =>
    updateRecord(domain, { ...defaultOpts, ...opts });
  setup2.updateHost = (host, domain, ip, opts = {}) =>
    updateHost(host, domain, ip, { ...defaultOpts, ...opts });
  setup2.records = (domain, opts = {}) =>
    records(domain, { ...defaultOpts, ...opts });
  return setup2;
}

export default setup();
