export default function err(msg, obj) {
  const stackLine = (new Error()).stack.split("\n")[2].trim();
  const moduleLine = stackLine.match(
    /^at \w+ \(.+(\b\w+\.(?:js|ts):\d+):\d+\)$/,
  )[1];
  return `${moduleLine}: ${msg} ${JSON.stringify(obj)}`;
}
