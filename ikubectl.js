import * as kubectl from "./kubectl.js";
import * as strings from "./strings.js";
import { parse } from "https://deno.land/std@0.148.0/flags/mod.ts";

export function ikubectlYargsCommands(opts) {
  const xKubectl = kubectl.setup(opts);
  return [
    {
      command: "kubectl [args...]",
      desc: "Kubectl",
      builder: {},
      handler: (args) => ikubectl({ ...opts, ...args, kubectl: xKubectl }),
    },
  ];
}

async function ikubectl(opts = {}) {
  const { args, kubectl } = opts;
  const [command, resource, ...rest] = args;
  const flag = parse(rest);
  if (
    ["get", "describe"].includes(command) &&
    /pod[s]/.test(resource) &&
    flag._.length == 1
  ) {
    const podPattern = flag._[0];
    delete flag._;
    const cmd = Object.entries(flag).map(([k, v]) =>
      k.length == 1 ? `-${k} '${v}'` : `--${k} '${v}'`
    ).join(" ");
    const res = await kubectl.getPods({ pod: podPattern, running: false });
    const ns = {};
    for (const { pod, namespace } of res) {
      ns[namespace] ??= [];
      ns[namespace].push(pod);
    }
    for (const [namespace, pods] of Object.entries(ns)) {
      await kubectl(
        `${command} ${resource} ${cmd} ${pods.join(" ")}`,
        { verbosity: 2, namespace },
      );
    }
  } else {
    const cmd = strings.join(args, " ");
    await kubectl(`${cmd}`, { verbosity: 2 });
  }
}
