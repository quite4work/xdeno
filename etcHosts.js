import { sudoReplace } from "./editFile.js";

export async function set(ip, hosts) {
  const line = `${ip} ${hosts.join(" ")}`;
  const regex = new RegExp(
    `(#+\\s*)?\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\s+${
      hosts.join("\\s+")
    }`,
  );
  const res = await sudoReplace("/etc/hosts", regex, line);
  if (!res) {
    const cmd = `echo '${line}' >> /etc/hosts`;
    console.log(`sudo sh -c '${cmd}'`);
    await Deno.spawn("sudo", { args: ["sh", "-c", cmd] });
    return false;
  }
  return true;
}

export async function unset(ip, hosts) {
  const regex = new RegExp(`(#+\\s*)?${ip}\\s+${hosts.join("\\s+")}`);
  return await sudoReplace("/etc/hosts", regex, "");
}
