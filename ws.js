import * as server from "https://deno.land/std@0.148.0/http/server.ts";

function defaultOnopen() {
  console.log("Connected to client ...");
}

function defaultOnmessage(m) {
  console.log(`Message received: ${m}`);
}

function defaultOnclose() {
  console.log("Disconnected from client ...");
}

function defaultOnerror(e) {
  console.log(`Error: ${e}`);
}

function defaultHandleHttp() {
  return new Response("ok");
}

function handle(req, opts) {
  const {
    onmessage = defaultOnmessage,
    onopen = defaultOnopen,
    onclose = defaultOnclose,
    onerror = defaultOnerror,
    handleHttp = defaultHandleHttp,
  } = opts;
  if (req.headers.get("upgrade") === "websocket") {
    const { socket: ws, response } = Deno.upgradeWebSocket(req, {
      idleTimeout: 0,
    });
    ws.onopen = () => onopen(ws);
    ws.onmessage = (m) => onmessage(m, ws);
    ws.onclose = () => onclose(ws);
    ws.onerror = (e) => onerror(e, ws);
    return response;
  } else {
    return handleHttp(req);
  }
}

export async function start(opts) {
  const { port = 8080, timeout = 30000 } = opts;
  const controller = new AbortController();
  const timer = setTimeout(() => {
    controller.abort();
  }, timeout);
  await server.serve((req) => handle(req, { ...opts, timer }), {
    port,
    signal: controller.signal,
  });
}
