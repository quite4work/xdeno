export async function ip(host, networkInterface) {
  const cmd = `ip -f inet addr show ${networkInterface}`;
  const { stdout } = await Deno.spawn("ssh", {
    args: [host, cmd],
  });
  for (const line of new TextDecoder().decode(stdout).split("\n")) {
    const res = line.match(/inet (\d+\.\d+\.\d+\.\d+)/);
    if (res) {
      return res[1];
    }
  }
}

export async function sshWaitReady(host, opts) {
  const { retryInterval = 1000 } = opts;
  while (!await sshReady(host)) {
    console.log(`waiting ssh for ${host}`);
    await delay(retryInterval);
  }
}

export async function sshWaitAllReady(hosts, opts) {
  await Promise.all(hosts.map((host) => sshWaitReady(host, opts)));
}

export async function sshReady(host) {
  const { stdout } = await Deno.spawn("ssh", { args: [host, "whoami"] });
  const res = new TextDecoder().decode(stdout);
  if (res !== "core") {
    return false;
  }
  return true;
}

export async function sshAllReady(hosts) {
  if (hosts.length == 0) {
    return false;
  }
  const ready = await Promise.all(hosts.map(sshReady));
  return ready.every((v) => v === true);
}
