import { basename, join } from "https://deno.land/std@0.148.0/path/posix.ts";
import { includes, notIncludes } from "./strings.js";
import { ansi } from "https://deno.land/x/cliffy@v0.24.2/ansi/ansi.ts";
import { delay } from "https://deno.land/std@0.148.0/async/delay.ts";

const ctx = new URL(import.meta.url).searchParams.get("context");

export function kubectlYargsCommands(opts) {
  return [
    {
      command: "ssh-pod <pod>",
      desc: "SSH into Pod",
      builder: {
        host: { alias: "h", desc: "Host regex" },
        container: { alias: "c", desc: "Container regex" },
        namespace: { alias: "n", desc: "Namespace exact match" },
      },
      handler: ({ host, ...argv }) =>
        sshPod(argv.pod, {
          ...opts,
          ...argv,
          node: host,
        }),
    },
  ];
}

export async function apply(manifests, opts = {}) {
  const { context } = opts;
  for (const manifest of manifests) {
    await kubectl("apply", {
      verbosity: 1,
      context,
      stdin: JSON.stringify(manifest),
      args: ["-f", "-"],
    });
  }
}

export async function cp(
  src,
  pod,
  { namespace, dest = ".", context },
) {
  const ctx = context ? `--context=${context}` : "";
  const ns = namespace ? `-n ${namespace}` : "";
  const destFile = join(dest, basename(src));
  const { stdout } = await Deno.spawn("stat", { args: ["-c", "%a", src] });
  const mod = new TextDecoder().decode(stdout).trim();
  const cmd =
    `cat '${src}' | kubectl ${ctx} ${ns} exec -i '${pod}' -- sh -c "cat - > '${destFile}' && chmod ${mod} '${destFile}'"`;
  const { success } = await Deno.spawn("sh", { args: ["-c", cmd] });
  return success;
}

export async function createNamespace(namespace, opts) {
  const ok = await kubectl("get", {
    ...opts,
    ok: true,
    args: ["namespace", namespace, "-o", "name"],
  });
  if (!ok) {
    await kubectl("create", {
      ...opts,
      args: ["namespace", namespace],
    });
  }
}

export async function getFirstPod(pod, opts = {}) {
  const { verbosity, namespace, node } = opts;
  const verbosity2 = verbosity - 2 > 0 ? verbosity - 2 : 0;
  let pods = await getPods({ ...opts, pod, verbosity: verbosity2 });
  if (pods.length == 0) {
    if (verbosity) {
      console.error(`pod not found`);
    }
    return;
  }
  pods = pods.filter(({ node: n, namespace: ns }) =>
    includes(n, node) && includes(ns, namespace)
  );
  if (pods.length > 1) {
    if (verbosity > 1) {
      console.error("Selecting first pod of:");
      for (const { namespace, pod, node } of pods) {
        console.error(`- ${pod} ${namespace} ${node}`);
      }
    }
  }
  return pods[0];
}

export async function sshPod(podPattern, opts = {}) {
  const { cmd, node, container, context, namespace: nsPattern } = opts;
  const { pod, namespace } = await getFirstPod(podPattern, {
    verbosity: 2,
    node,
    context,
    namespace: nsPattern,
  });
  const hasBash = await exec(pod, "bash", {
    verbosity: 0,
    container,
    namespace,
    context,
    ok: true,
  });
  const shell = hasBash ? "bash" : "sh";
  return await exec(pod, cmd ? cmd : shell, {
    verbosity: 2,
    container,
    context,
    namespace,
    interactive: true,
  });
}

export async function exec(pod, cmd, opts = {}) {
  const { interactive, container, namespace, context } = opts;
  const args = [pod];
  if (interactive) {
    args.push("-it");
  }
  return await kubectl("exec", {
    context,
    namespace,
    interactive,
    container,
    args,
    "--": cmd,
  });
}

export async function execAllPods(cmd, opts = {}) {
  const pods = await getPods(opts);
  const jobs = [];
  for (const { pod, namespace: podNamespace } of pods) {
    const job = kubectl("exec", {
      ...opts,
      args: [pod],
      "--": cmd,
      namespace: podNamespace,
    });
    jobs.push(job);
  }
  return await Promise.all(jobs);
}

export async function get(type, name = "", opts = {}) {
  const names = Array.isArray(name) ? name : [name];
  return await kubectl("get", {
    ...opts,
    args: [type, ...names],
    json: true,
  });
}

export async function isExists(type, name = "", opts = {}) {
  const names = Array.isArray(name) ? name : [name];
  return await kubectl("get", {
    ...opts,
    args: [type, ...names, "-o", "name"],
    ok: true,
  });
}

export async function getPods(opts = {}) {
  const {
    running = true,
    pod: podPattern,
    notPod,
    node: nodePattern,
    notNode,
    namespace,
  } = opts;
  const fields = [
    ".metadata.name",
    ".metadata.namespace",
    ".spec.nodeName",
    ".status.phase",
    ".metadata.deletionTimestamp",
  ].map((f) => `{${f}}`).join(`{"|||"}`) + `{"\\n"}`;
  const jsonpath = `{range .items[*]}${fields}{end}`;
  const args = ["pods", "-o", `jsonpath='${jsonpath}'`];
  if (running) {
    args.push("--field-selector");
    args.push("status.phase=Running");
  }
  const podsRaw = await kubectl("get", {
    defaultAllNamespaces: true,
    ...opts,
    args,
    namespace: undefined,
    verbosity: 0,
  });
  const map = (s) => {
    const [pod, namespace, node, phase, deletionTimestamp] = s.split("|||");
    let status;
    if (deletionTimestamp) {
      status = "Terminating";
    } else if (phase === "Pending") {
      status = "Pending";
    } else if (phase === "Running") {
      status = "Running";
    }
    return ({ pod, namespace, node, status });
  };
  const filter = ({ node, pod, namespace: ns }) =>
    includes(ns, namespace) &&
    includes(pod, podPattern) &&
    includes(node, nodePattern) &&
    notIncludes(pod, notPod) &&
    notIncludes(node, notNode);
  const pods = podsRaw.split("\n").map(map).filter(filter);
  return pods.sort(({ pod, node }, { pod: pod2, node: node2 }) => {
    let res = 0;
    if (node < node2) {
      res--;
    }
    if (node > node2) {
      res++;
    }
    if (pod.length < pod2.length) {
      res--;
    }
    if (pod.length > pod2.length) {
      res++;
    }
    return res;
  });
}

export async function getNodesNames(opts) {
  const res = await kubectl("get", {
    ...opts,
    args: ["nodes", "-o", "name"],
    lines: true,
    verbosity: 0,
  });
  return res.map((n) => n.match(/^node\/(.*)$/)[1]);
}

export async function isContextExists(context, opts) {
  return await kubectl("config", {
    ...opts,
    args: ["get-contexts", context, "-o", "name"],
    ok: true,
  });
}

export async function isNodesReady(opts = {}) {
  let { nodes, verbosity } = opts;
  let items;
  try {
    const res = await get("nodes", nodes, opts);
    items = res.items ? res.items : [res];
  } catch (e) {
    if (verbosity > 2) {
      console.log(e);
    }
    return false;
  }
  let ready = items.length > 0;
  for (const node of items) {
    const { metadata: { name }, status: { conditions } } = node;
    if (nodes) {
      const found = nodes.includes(name);
      nodes = nodes.filter((n) => n !== name);
      if (!found) {
        continue;
      }
    }
    for (const { status, type } of conditions) {
      switch (type) {
        case "NetworkUnavailable":
        case "MemoryPressure":
        case "DiskPressure":
        case "PIDPressure":
          ready &&= status === "False";
          break;
        case "Ready":
          ready &&= status === "True";
          break;
        default:
          throw `unknown condition: ${type}`;
      }
    }
  }
  return ready && (!nodes || nodes.length === 0);
}

export async function waitPodsReady(opts) {
  const opts2 = { defaultAllNamespaces: true, ...opts };
  const ready = kubectl("wait", {
    ...opts2,
    args: ["--for", "condition=ready", "pod", "--all"],
  });
  let lines = 0;
  let ok = 0;
  while (ok < 10) {
    const out = await kubectl("get", { ...opts2, args: ["pod"] });
    const eraser = ansi.cursorUp(lines).cursorLeft.eraseDown(lines).toString();
    console.log(eraser, out);
    lines = out.split("\n").length;
    if (await isFinished(ready)) {
      ok++;
    } else {
      ok = 0;
    }
  }
}

function isFinished(promise, msDelay = 1000) {
  return Promise.race([
    delay(msDelay),
    promise.then(() => true, () => true),
  ]);
}

export async function delete_(type, name, opts = {}) {
  return await kubectl("delete", {
    ...opts,
    args: [type, name],
    namespace: null,
  });
}

export async function labelNodes(nodes, labels = {}, opts = {}) {
  const labels2 = Object.entries(labels)
    .map(([label, value]) =>
      value === null ? `${label}-` : `${label}=${value}`
    );
  const nodes2 = Array.isArray(nodes) ? nodes : [nodes];
  return await kubectl("label", {
    ...opts,
    args: ["node", "--overwrite", ...nodes2, ...labels2],
    namespace: null,
  });
}

export async function kubectl(cmd, opts = {}) {
  const {
    ok,
    lines,
    args,
    stdin: stdinText,
    interactive,
    container,
    context = ctx,
    namespace,
    json,
    defaultAllNamespaces,
    verbosity,
    "--": extraCmd,
    kubectl: kubectlBin = "kubectl",
  } = opts;
  if (cmd === "edit") {
    throw "edit is not supported";
  }
  const args2 = [cmd];
  if (context) {
    args2.push("--context");
    args2.push(context);
  }
  if (namespace && namespace !== "default") {
    args2.push("-n");
    args2.push(namespace);
  } else if (defaultAllNamespaces) {
    args2.push("-A");
  }
  if (json) {
    args2.push("-o");
    args2.push("json");
  }
  if (container) {
    args2.push("-c");
    args2.push(container);
  }
  args2.push(...args);
  if (extraCmd) {
    args2.push("--");
    args2.push(extraCmd);
  }
  let stdout;
  let stderr;
  let stdin;
  if (stdinText) {
    stdin = "piped";
  }
  if (verbosity) {
    stdout = "inherit";
    stderr = "inherit";
  }
  if (verbosity > 1) {
    console.log([kubectlBin, ...args2].join(" "));
  }
  if (interactive) {
    stdout = "inherit";
    stderr = "inherit";
    stdin = "inherit";
  }
  const child = await Deno.spawnChild(kubectlBin, {
    args: args2,
    stdout,
    stderr,
    stdin,
  });
  if (stdin === "piped") {
    const writer = child.stdin.getWriter();
    await writer.write(new TextEncoder().encode(stdinText));
    writer.releaseLock();
    child.stdin.close();
  }
  if (ok || verbosity || interactive) {
    const { success } = await child.status;
    return success;
  }
  const { stdout: out } = await child.output();
  const text = new TextDecoder().decode(out).trim();
  if (lines) {
    return text.split("\n");
  } else {
    return json ? JSON.parse(text) : text;
  }
}

export function patch(type, name, opts = {}) {
  return {
    replace(path, value) {
      this.ops = this.ops ?? [];
      this.ops.push({ op: "replace", path, value });
      return this;
    },
    remove(path) {
      this.ops = this.ops ?? [];
      this.ops.push({ op: "remove", path });
      return this;
    },
    async apply() {
      const { verbosity = 0 } = opts;
      const patch = JSON.stringify(this.ops);
      const res = await kubectl(
        "path",
        { ...opts, args: [type, name, "--type", "json", "-p", patch] },
      );
      if (verbosity == 1) {
        console.log(res);
      }
      return res;
    },
  };
}
