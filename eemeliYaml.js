import yaml from "https://esm.sh/yaml@2.1.1";

// NOTE: Do not remove. Example of importing not public module.
// import {
//   stringifyCollection,
// } from "https://dev.jspm.io/npm:yaml@undefined/browser/dist/stringify/stringifyCollection.js";

// Format: 'QUOTE_DOUBLE' ⎮ 'QUOTE_SINGLE' ⎮ 'PLAIN'
export function setFormat(doc, format) {
  yaml.visit(doc, {
    Scalar(_, node) {
      if (typeof node.value === "string") {
        node.type = format;
      }
    },
  });
}

export function deleteKeys(doc, keys) {
  yaml.visit(doc, {
    Pair(_, pair) {
      if (keys.includes(pair.key.value)) {
        return yaml.visit.REMOVE;
      }
    },
  });
}

export function replacePrefix(doc, prefix, replacement) {
  yaml.visit(doc, {
    Pair(_, pair) {
      if (
        pair.value && pair.value.value &&
        typeof pair.value.value == "string"
      ) {
        if (pair.value.value.startsWith(prefix)) {
          pair.value.value = pair.value.value.replace(prefix, replacement);
        }
      }
    },
  });
}

export function replace(doc, string, replacement) {
  yaml.visit(doc, {
    Pair(_, pair) {
      if (
        pair.value && pair.value.value &&
        typeof pair.value.value == "string"
      ) {
        if (pair.value.value === string) {
          pair.value.value = pair.value.value.replace(string, replacement);
        }
      }
    },
  });
}

// map:                map:
//   src: val     ->     dest: val2
//   dest: val2          src: val
export function moveKeyAfter(doc, src, dest) {
  const src2 = [...src];
  const dest2 = [...dest];

  const srcPair = getPair(doc, src2);
  doc.deleteIn(src2);
  src2.pop();

  const after = dest2.pop();
  const destMap = doc.getIn(dest2, true);

  const res = [];
  for (const pair of destMap.items) {
    res.push(pair);
    if (pair.key.value === after) {
      res.push(srcPair);
    }
  }
  destMap.items = res;
}

export function getPair(doc, path) {
  const path2 = [...path];
  const key = path2.pop();
  const map = doc.getIn(path2, true);
  for (const pair of map.items) {
    if (pair.key.value === key) {
      return pair;
    }
  }
}
