import { decode, encode } from "https://deno.land/std@0.148.0/encoding/hex.ts";

export async function hash(any, algorithm = "SHA-512") {
  return new TextDecoder().decode(encode(
    new Uint8Array(
      await crypto.subtle.digest(
        algorithm,
        new TextEncoder().encode(any.toString()),
      ),
    ),
  ));
}

export async function encrypt(key, val) {
  const iv = crypto.getRandomValues(new Uint8Array(16));
  const cipher = await crypto.subtle.encrypt(
    {
      name: "AES-CBC",
      iv,
    },
    await deriveKey(key),
    new TextEncoder().encode(val),
  );
  const cipherHex = new TextDecoder().decode(encode(new Uint8Array(cipher)));
  const ivHex = new TextDecoder().decode(encode(iv));
  return cipherHex + "_" + ivHex;
}

export async function decrypt(key, val) {
  const [cipherHex, ivHex] = val.split("_");
  const str = await crypto.subtle.decrypt(
    {
      name: "AES-CBC",
      iv: decode(new TextEncoder().encode(ivHex)),
    },
    await deriveKey(key),
    decode(new TextEncoder().encode(cipherHex)),
  );
  return new TextDecoder().decode(str);
}

async function deriveKey(key) {
  const keyHash = await hash(key);
  const key2 = await crypto.subtle.importKey(
    "raw",
    new TextEncoder().encode(keyHash.slice(0, 32)),
    "PBKDF2",
    false,
    ["deriveBits", "deriveKey"],
  );
  return await crypto.subtle.deriveKey(
    {
      name: "PBKDF2",
      salt: new TextEncoder().encode("salt"),
      iterations: 100000,
      hash: "SHA-256",
    },
    key2,
    { name: "AES-CBC", length: 256 },
    false,
    ["encrypt", "decrypt"],
  );
}
