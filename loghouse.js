import { MuxAsyncIterator } from "https://deno.land/std@0.148.0/async/mux_async_iterator.ts";
import { cloneDeep } from "https://cdn.skypack.dev/lodash-es@4.17.21";
import { delay } from "https://deno.land/std@0.148.0/async/delay.ts";
import { getKey } from "./object.js";
import { start as webStart } from "./web.js";
import { inferLevel, parse2, parseLevel } from "./log.js";
import { readCSVObjects } from "https://deno.land/x/csv@v0.7.5/reader.ts";
import { readerFromStreamReader } from "https://deno.land/std@0.148.0/streams/conversion.ts";
import { start as wsStart } from "./ws.js";

export function loghouseYargsCommands(opts) {
  const { url, auth, command = "$0" } = opts;
  return [{
    command,
    desc: "",
    handler: (argv) => start({ ...opts, ...argv }),
    builder: {
      since: {
        alias: "s",
        desc: "from = now - <since>",
        requiresArg: true,
        type: "string",
      },
      duration: {
        alias: "d",
        desc: "to = now - <since>",
        requiresArg: true,
        type: "string",
      },
      from: {
        alias: "f",
        desc: 'Time from "DD.MM.YYYY hh:mm"',
        default: "now-1m",
        requiresArg: true,
        type: "string",
      },
      to: {
        alias: "t",
        desc: 'Time to "DD.MM.YYYY hh:mm"',
        default: "now",
        requiresArg: true,
        type: "string",
      },
      url: {
        desc: "u",
        default: url,
        requiresArg: true,
        type: "string",
      },
      auth: {
        alias: "a",
        desc: "Auth token",
        default: auth,
        type: "string",
      },
      message: {
        alias: "m",
        desc: "Log message to search",
        type: "array",
      },
      notMessage: {
        alias: "M",
        desc: "Log message to exclude",
        type: "array",
      },
      pod: {
        alias: "p",
        desc: "Pod to search",
        type: "array",
      },
      notPod: {
        alias: "P",
        desc: "Pod not to search",
        type: "array",
      },
      verbosity: {
        alias: "v",
        desc: "Verbosity",
        type: "count",
      },
      namespace: {
        desc: "Filter by namespace",
        alias: "n",
        requiresArg: true,
        type: "array",
      },
      notNamespace: {
        desc: "exclude namespace",
        alias: "N",
        requiresArg: true,
        type: "array",
      },
      host: {
        desc: "Filter by host",
        alias: "h",
        requiresArg: true,
        type: "array",
      },
      query: {
        desc: "Loghouse Query",
        alias: "q",
        requiresArg: true,
        type: "string",
      },
    },
  }];
}

async function start(opts = {}) {
  await webStart(run, opts);
}

async function run(opts) {
  const { url: urls } = opts;

  const query = makeQuery(opts);
  console.log(`Query: ${query}`);

  let logs;
  if (Array.isArray(urls)) {
    logs = new MuxAsyncIterator();
    for (const url of urls) {
      logs.add(await runQuery(query, { ...opts, url }));
    }
    logs = logs[Symbol.asyncIterator]();
    await delay(1000);
  } else {
    logs = await runQuery(query, opts);
  }
  const { value } = await logs.next();
  if (!value) {
    console.log("Empty resp");
    Deno.exit(0);
  }

  return new Promise((resolve) => {
    wsStart({
      ...opts,
      onopen: async (ws) => {
        resolve();
        await onopen(value, logs, ws);
      },
    });
  });
}

async function onopen(value, logs, ws) {
  ws.send(JSON.stringify(formatLog(value)));
  for await (const l of logs) {
    ws.send(JSON.stringify(formatLog(l)));
  }
}

export function makeQuery(opts = {}) {
  const {
    pod,
    notPod,
    query,
    namespace,
    notNamespace,
    message,
    notMessage,
    host,
  } = opts;
  let res = query ?? "";
  if (host) {
    if (res) {
      res += ` and `;
    }
    res += host
      .map((h) => `host = "%${h}%"`)
      .join(" or ");
  }
  if (namespace) {
    if (res) {
      res += ` and `;
    }
    res += namespace
      .map((n) => `namespace = "%${n}%"`)
      .join(" or ");
  }
  if (notNamespace) {
    if (res) {
      res += ` and `;
    }
    res += notNamespace
      .map((nn) => `namespace != "%${nn}%"`)
      .join(" and ");
  }
  if (pod) {
    if (res) {
      res += ` and `;
    }
    res += pod
      .map((pod) => `pod_name = "%${pod}%"`)
      .join(" or ");
  }

  if (notPod) {
    if (res) {
      res += ` and `;
    }
    res += notPod
      .map((nPod) => `pod_name != "%${nPod}%"`)
      .join(" and ");
  }
  if (notMessage) {
    if (res) {
      res += ` and `;
    }
    res += notMessage
      .map((msg) => `log != "%${msg}%"`)
      .join(" and ") +
      " or " + res +
      notMessage
        .map((msg) => `msg != "%${msg}%"`)
        .join(" and ");
  }
  if (message) {
    if (res) {
      res += ` and `;
    }
    res += message
      .map((msg) => `log = "%${msg}%"`)
      .join(" and ") +
      " or " + res +
      message.map((msg) => `msg = "%${msg}%"`).join(" and ");
  }
  return res;
}

export async function runQuery(query, opts) {
  const { url, auth, since, duration } = opts;
  let { from, to } = opts;
  if (!since && duration) {
    from = "now-" + duration;
    to = "now";
  }
  if (since && !duration) {
    const [, s_number, s_suffix] = since.match(/(\d+)([hms])/);
    const t_number = Number(s_number) - 1;
    from = "now-" + since;
    to = t_number > 0 ? "now-" + t_number + s_suffix : "now";
  }
  if (since && duration) {
    const [, s_number, s_suffix] = since.match(/(\d+)([hms])/);
    const [, d_number, d_suffix] = duration.match(/(\d+)([hms])/);
    if (s_suffix != d_suffix) {
      throw `Suffixes should be same: ${s_suffix} != ${d_suffix}`;
    }
    const t_number = Number(s_number) - Number(d_number);
    from = "now-" + since;
    to = t_number > 0 ? "now-" + t_number + s_suffix : "now";
  }
  console.log({ from, to });
  const qsObj = {
    time_format: "range",
    seek_to: "",
    time_from: from,
    time_to: to,
    query,
    per_page: "",
  };
  const qsStr = new URLSearchParams(toQs(qsObj)).toString();
  const res = await fetch(
    `${url}/query.csv?${qsStr}`,
    {
      credentials: "include",
      headers: {
        "Authorization": `Basic ${auth}`,
      },
      mode: "cors",
    },
  );
  if (res.status != 200) {
    console.log(`Error response: ${res.status}`);
    return;
  }
  return readCSVObjects(readerFromStreamReader(res.body.getReader()));
}

function toQs(obj) {
  return Object.entries(obj).flatMap(([k, v]) => {
    if (Array.isArray(v)) {
      return v.map((v) => [k + "[]", v]);
    } else {
      return [[k, v]];
    }
  });
}

export function formatLog(l0) {
  const l = cloneDeep(l0);
  const { timestamp, pod_name, log, msg: msg0, host, container_name } = l;
  const ts = new Date(timestamp);
  delete l.timestamp;
  delete l.pod_name;
  delete l.host;
  delete l.namespace;
  delete l.container_name;
  delete l.source;
  delete l.stream;
  for (const [k, v] of Object.entries(l)) {
    if (v === "" || k.startsWith("~")) {
      delete l[k];
    }
  }

  let msg;
  let inferredLevel = "";
  let level = "";
  if (log && Object.keys(l).length === 1) {
    msg = log;
    const parsed = parse2(log);
    if (parsed) {
      const levelKey = getKey(parsed, parseLevel);
      level = parseLevel(parsed[levelKey]) ?? "";
    }
    inferredLevel = inferLevel(log, "");
  } else if (msg0) {
    delete l.msg;
    msg = msg0 + " " + JSON.stringify(l);
    const levelKey = getKey(l, parseLevel);
    level = parseLevel(l[levelKey]) ?? "";
    inferredLevel = inferLevel(msg0, "");
  } else {
    msg = JSON.stringify(l);
    const levelKey = getKey(l, parseLevel);
    level = parseLevel(l[levelKey]) ?? "";
    inferredLevel = inferLevel(msg, "");
  }

  return {
    ts,
    host,
    pod: pod_name,
    container: container_name,
    msg,
    level,
    inferredLevel,
    cols: [
      "level",
      "inferredLevel",
      "host",
      "pod",
      "container",
      "ts",
      "msg",
    ],
  };
}
