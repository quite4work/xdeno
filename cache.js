import { decrypt, encrypt, hash } from "./crypt.js";

export function clear() {
  Object.keys(sessionStorage)
    .filter((k) => k.startsWith("cache@" + Deno.mainModule + "="))
    .forEach((k) => sessionStorage.removeItem(k));
}

export async function get(key) {
  const hsh = await hash(key + Deno.mainModule);
  const encrKey = hsh.slice(0, 32);
  const cipher = sessionStorage.getItem(
    "cache@" + Deno.mainModule + "=" + hsh,
  );
  if (cipher) {
    return JSON.parse(await decrypt(encrKey, cipher));
  }
}

export async function set(key, val) {
  if (!val && (typeof val != "boolean")) {
    // Do not save empty values unless it is boolean `false`
    return;
  }
  const hsh = await hash(key + Deno.mainModule);
  const encrKey = hsh.slice(0, 32);
  const cipher = await encrypt(encrKey, JSON.stringify(val));
  sessionStorage.setItem("cache@" + Deno.mainModule + "=" + hsh, cipher);
}
