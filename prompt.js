import * as prompts from "https://cdn.jsdelivr.net/gh/quite4work/deno-prompts@0.0.4/mod.ts";

export async function secret(prompt) {
  let secret = await prompts.promptSecret(prompt);
  secret = secret.trim();
  if (secret) {
    return secret;
  }
}
