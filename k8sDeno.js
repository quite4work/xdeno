import { daemonSet as k8sDaemonSet, service } from "./k8s.js";
import { apply, createNamespace, delete_, get } from "./kubectl.js";

function daemonSet(name, src, opts) {
  const { port = 15616 } = opts;
  const command = [
    "sh",
    "-c",
    `echo ${JSON.stringify(src)} | deno run --unstable -A -`,
  ];
  const ds = k8sDaemonSet(name, {
    ...opts,
    image: "denoland/deno",
    command,
    ports: { default: port },
  });
  return [ds];
}

async function pingDeploy(opts = {}) {
  const { namespace = "deno-ping", port = 15616 } = opts;
  await createNamespace(namespace, opts);
  const { items } = await get("nodes", "", opts);
  const externalIPs = items.flatMap(({ status: { addresses } }) => {
    for (const { address, type } of addresses) {
      if (type == "InternalIP") {
        return address;
      }
    }
  });
  const pingSrc = `
import * as ping from "https://gitlab.com/quite4work/xdeno/-/raw/master/ping.js";
await ping.start({interval: 100, port: ${port}, hosts: ${
    JSON.stringify(externalIPs)
  }})
`;
  const app = [
    ...daemonSet("ping", pingSrc, opts),
    service("ping", "ping", opts),
  ];
  await apply(app, opts);
}

async function pingClean(opts = {}) {
  const { namespace = "deno-ping" } = opts;
  await delete_("ns", namespace, opts);
}

export function k8sDenoYargsCommands(opts) {
  const { namespace = "deno-ping" } = opts;
  return [
    {
      command: "deno ping deploy",
      desc: "Deploy Ping checks",
      builder: {
        namespace: { alias: "ns", default: namespace },
        verbosity: { alias: "v", type: "number" },
      },
      handler: (argv) =>
        pingDeploy({
          ...opts,
          ...argv,
        }),
    },
    {
      command: "deno ping clean",
      desc: "Remove Ping checks",
      builder: {
        namespace: { alias: "ns", default: namespace },
        verbosity: { alias: "v", type: "number" },
      },
      handler: (argv) =>
        pingClean({
          ...opts,
          ...argv,
        }),
    },
  ];
}
