// Overrides object methods
// https://2ality.com/2015/10/intercepting-method-calls.html
// https://javascript.info/proxy#proxy-limitations
export function intercept(obj, methods) {
  const handler = {
    get(target, prop, _receiver) {
      const value = Reflect.get(...arguments);

      if (typeof value == "function") {
        if (methods[prop]) {
          return function (...args) {
            const orig = (...args) => value.apply(this, args);
            return methods[prop](orig, ...args);
          };
        } else {
          return value.bind(target);
        }
      } else {
        return value;
      }
    },
  };

  return new Proxy(obj, handler);
}

// Example of using intercept for tracing methods calls.
export function trace(obj, method) {
  const methods = {
    [method]: (orig, ...args) => {
      const result = orig(args);
      console.log({ trace: method, args, result });
      return result;
    },
  };
  return intercept(obj, methods);
}
