// https://github.com/denoland/deno/issues/14176
let doneClose;
function close(callback) {
  if (doneClose) {
    return;
  }
  doneClose = true;
  callback();

  Deno.exit(0);
}

// TODO: doesn't work
export function onExit(callback) {
  // Binding to signal 'SIGSTOP', 'SIGKILL' is not allowed
  const signals = ["SIGINT", "SIGTERM", "SIGQUIT", "SIGHUP"];
  for (const signal of signals) {
    Deno.addSignalListener(signal, () => close(callback));
  }
  addEventListener("unload", () => close(callback));
  globalThis.onunload = () => close(callback);
}
