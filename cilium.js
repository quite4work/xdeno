import { createNamespace, delete_, kubectl } from "./kubectl.js";

async function checksDeploy(opts = {}) {
  const { ciliumVer, namespace = "cilium-checks" } = opts;
  await createNamespace(namespace, opts);
  const url =
    `https://raw.githubusercontent.com/cilium/cilium/${ciliumVer}/examples/kubernetes/connectivity-check/connectivity-check.yaml`;
  await kubectl(`apply -f ${url}`, opts);
}

async function checksClean(opts = {}) {
  const { namespace = "cilium-checks" } = opts;
  await delete_("ns", namespace, opts);
}

export function ciliumYargsCommands(opts = {}) {
  const { namespace = "cilium-checks" } = opts;
  return [
    {
      command: "cilium-checks deploy",
      desc: "Deploy Cilium checks",
      builder: {
        namespace: { alias: "ns", default: namespace },
        verbosity: { alias: "v", type: "number" },
        ciliumVer: { alias: "V", default: "master" },
      },
      handler: (argv) =>
        checksDeploy({
          ...opts,
          ...argv,
        }),
    },
    {
      command: "cilium-checks clean",
      desc: "Remove Cilium checks",
      builder: {
        namespace: { alias: "ns", default: namespace },
        verbosity: { alias: "v", type: "number" },
      },
      handler: (argv) =>
        checksClean({
          ...opts,
          ...argv,
        }),
    },
  ];
}
