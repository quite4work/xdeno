import {
  KubeConfig,
  KubeConfigRestClient,
  ReadLineTransformer,
} from "https://deno.land/x/kubernetes_client@v0.4.0/mod.ts";
import { readLines } from "https://deno.land/std@0.148.0/io/buffer.ts";
import { alive, link } from "./proc.js";
import Storage from "./storage.js";

const proxies = new Storage(import.meta.url);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/#read-log-pod-v1-core
export async function tailContainerLogs(opts = {}) {
  const {
    namespace = "default",
    pod,
    follow,
    timestamps,
    container,
    previous,
    sinceSeconds,
    context,
  } = opts;
  const qs = {
    follow,
    timestamps,
    container,
    previous,
    sinceSeconds,
  };
  return await request({
    context,
    path: `/api/v1/namespaces/${namespace}/pods/${pod}/log`,
    expectJson: false,
    expectStream: true,
    method: "GET",
    qs,
  });
}

export async function getPods(opts = {}) {
  const { namespace, context, watch } = opts;
  const qs = { watch };
  return await request({
    context,
    path: namespace ? `/api/v1/namespaces/${namespace}/pods` : `/api/v1/pods`,
    expectJson: true,
    expectStream: watch,
    method: "GET",
    qs,
  });
}

async function startKubectlProxy(context) {
  const ctx = proxies[context];
  if (ctx) {
    if (await alive(ctx.pid)) {
      return ctx.port;
    }
  }
  const p = Deno.run({
    cmd: ["kubectl", "proxy", `--port=0`, `--context=${context}`],
    stdin: "null",
    stderr: "piped",
    stdout: "piped",
  });
  // if (
  //   typeof WorkerGlobalScope === "undefined" ||
  //   !(self instanceof WorkerGlobalScope)
  // ) {
  //   console.log(1, ctx);
  link(p.pid);
  // }
  for await (const line of readLines(p.stdout)) {
    const match = line.match(/Starting to serve on 127.0.0.1:(\d+)/);
    if (match) {
      const port = new Number(match[1]);
      proxies[context] = { port, pid: p.pid };
      return port;
    }
  }
  for await (const line of readLines(p.stderr)) {
    if (line.match(/address already in use/)) {
      throw "address already in use";
    }
  }
  throw "error starting kubectl proxy";
}

export async function request(opts = {}) {
  const { context, qs = {}, path, expectJson, expectStream, method } = opts;
  const port = await startKubectlProxy(context);
  const qs2 = Object.fromEntries(
    Object.entries(qs).filter(([, v]) => v !== undefined),
  );
  let opts2 = { path, expectJson, expectStream, method };
  if (Object.keys(qs2).length > 0) {
    opts2 = { ...opts2, querystring: new URLSearchParams(qs2) };
  }
  const client = await KubeConfigRestClient.forKubeConfig(
    KubeConfig.getSimpleUrlConfig({ baseUrl: `http://localhost:${port}` }),
  );
  const res = await client.performRequest(opts2);
  if (expectJson) {
    return res;
  } else {
    return await res.pipeThrough(new ReadLineTransformer("utf-8"));
  }
}
