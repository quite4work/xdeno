import { Webview } from "https://deno.land/x/webview@0.7.3/mod.ts";
import javascript from "https://gitlab.com/xdeno/utils/-/raw/0.0.1/literal.js";

export async function openHtml(html, init) {
  return await open(`data:text/html,${encodeURIComponent(html)}`, init);
}

// https://github.com/webview/webview/issues/397
const fixCopyAndPaste = javascript`
globalThis.addEventListener("keypress", (event) => {
  if (event.metaKey && event.key === "c") {
    document.execCommand("copy");
    event.preventDefault();
  }
  if (event.metaKey && event.key === "v") {
    document.execCommand("paste");
    event.preventDefault();
  }
});
`;

export async function open(uri, init) {
  const webview = new Webview(true, {
    width: 1280,
    height: 800,
  });
  if (init) {
    await init(webview);
  }
  webview.navigate(uri);
  webview.init(fixCopyAndPaste);
  webview.run();
}
