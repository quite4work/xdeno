export async function bundle(src) {
  const tmp = await Deno.makeTempFile();
  await Deno.writeTextFile(tmp, src);
  try {
    await Deno.spawn(Deno.execPath(), { args: ["bundle", tmp] });
  } finally {
    await Deno.remove(tmp);
  }
}
