export function isFinished(promise) {
  return Promise.race([just(false), promise.then(() => true, () => true)]);
}

export function just(value) {
  return new Promise((res) => setTimeout(() => res(value), 0));
}
