import {
  stringify as yStringify,
} from "https://deno.land/std@0.148.0/encoding/yaml.ts";
export {
  parse,
  parseAll,
} from "https://deno.land/std@0.148.0/encoding/yaml.ts";

export function stringifyAll(obj, opts) {
  return "---\n" + obj.map((o) => stringify(o, opts)).join("\n---\n");
}

export function stringify(obj, opts) {
  return yStringify(obj, {
    noRefs: true, // don't convert duplicate objects into references
    noCompatMode: true, // don't quote "yes", "no" and so on, as required for YAML 1.1
    ...opts,
  });
}
