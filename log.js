import * as re from "./re.js";
import * as logfmt from "./logfmt.js";

export function parse2(text) {
  try {
    return JSON.parse(text);
  } catch {
    const match = text.match(re.klog());
    if (match) {
      return klog(match);
    } else {
      const match = text.match(re.logfmt());
      if (match) {
        try {
          return logfmt.parse(text);
        } catch {
          return;
        }
      } else {
        return;
      }
    }
  }
}

function klog(match) {
  const { level, msg, file, line, day, month, time } = match.groups;
  const year = new Date().getFullYear();
  return {
    level: parseLevel(level),
    msg: parse2(msg),
    caller: `${file}:${line}`,
    ts: new Date(`${year}-${month}-${day}T${time}`),
  };
}

export function showLevel(level, maxLevel, defaultLevel = "info") {
  return levelPriority(level, defaultLevel) >=
    levelPriority(maxLevel, defaultLevel);
}

export function levelPriority(level, defaultLevel = "info") {
  switch (level) {
    case "debug":
      return 1;
    case "info":
      return 2;
    case "warning":
      return 3;
    case "error":
      return 4;
    default:
      return levelPriority(defaultLevel);
  }
}

export function inferLevel(msg, defaultLevel = "info") {
  const errorKeywords = [
    "err",
    "error",
    "fatal",
    "fail",
    "failed",
    "failure",
    "crit",
    "critical",
    "timeout",
    "unready",
    "not ready",
    "not healthy",
    "unhealthy",
    "unable",
    "canceled",
    "not found",
    "can't",
    "can not",
    "unreach",
    "not reachable",
    "unreachable",
    "not available",
    "unavailable",
    "deadline exceeded",
    "\\bbad\\b",
  ];
  const warningKeywords = ["warn", "warning"];
  const errorRegex = new RegExp(errorKeywords.join("|"), "ig");
  const warningRegex = new RegExp(warningKeywords.join("|"), "ig");
  if (errorRegex.test(msg)) {
    return "error";
  }
  if (warningRegex.test(msg)) {
    return "warning";
  }
  return defaultLevel;
}

export function parseLevel(v) {
  switch (v) {
    case "I":
    case "INFO":
    case "N":
    case "NOTICE":
    case "info":
    case "notice":
      return "info";
    case "D":
    case "DEBUG":
    case "debug":
      return "debug";
    case "W":
    case "WARN":
    case "WARNING":
    case "warn":
    case "warning":
      return "warning";
    case "E":
    case "ERR":
    case "ERROR":
    case "FATAL":
    case "err":
    case "error":
    case "fatal":
      return "error";
    default:
      return;
  }
}

export function isTimestamp(v) {
  return (isDate(v) || re.dateTime().test(v)) && isRecentYear(v);
}

function isRecentYear(date) {
  const diff = new Date().getFullYear() - new Date(date).getFullYear();
  return diff >= 0 && diff <= 1;
}

function isDate(value) {
  return !isNaN(new Date(value).getDate());
}

export function stripTimestamp(s) {
  return s.replace(re.anyTs(), "");
}
