import { serve } from "https://deno.land/std@0.148.0/http/server.ts";
import { writeAll } from "https://deno.land/std@0.148.0/streams/conversion.ts";

export async function start({ port, interval = 1000, hosts }) {
  const server = serve({ port });

  setInterval(() => ping(hosts, port), interval);

  for await (const request of server) {
    const body = "Deno Pong\n";
    request.respond({ status: 200, body });
  }
}

async function ping(hosts, port) {
  await writeAll(
    Deno.stdout,
    new TextEncoder().encode(`ping: `),
  );
  let total = 0;
  for (const host of hosts) {
    const startTime = performance.now();
    let resp;
    try {
      const res = await fetch(`http://${host}:${port}`);
      resp = await res.text();
    } catch (e) {
      resp = e.toString();
    }
    const err = (resp === "Deno Pong\n") ? "     " : "error";
    const time = Math.round(performance.now() - startTime);
    total += time;
    await writeAll(
      Deno.stdout,
      new TextEncoder().encode(`${host}${err}${time} `),
    );
  }
  console.log(`total: ${total}`);
}
