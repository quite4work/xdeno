const pid = new URL(import.meta.url).searchParams.get("watch");

if (pid) {
  watch(pid);
}

export function link(pid) {
  const src = `${import.meta.url}?watch=${pid}`;
  Deno.spawnChild(Deno.execPath(), {
    args: ["run", "--unstable", "--allow-run", src],
  });
}

function watch(pid) {
  setInterval(async () => {
    const pidAlive = await alive(pid);
    const parentAlive = await alive(Deno.ppid);
    if (!pidAlive || !parentAlive) {
      if (pidAlive) {
        Deno.kill(pid, "SIGKILL");
      }
      if (parentAlive) {
        Deno.kill(Deno.ppid, "SIGKILL");
      }
      Deno.exit(0);
    }
  }, 1000);
}

export async function alive(pid) {
  const { success } = await Deno.spawn("kill", {
    args: ["-0", pid],
  });
  return success;
}
