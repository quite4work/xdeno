import { resolve } from "https://deno.land/std@0.148.0/path/posix.ts";
import { encode } from "https://deno.land/std@0.148.0/encoding/hex.ts";
import Storage from "./storage.js";

export async function changed(file) {
  const absPath = resolve(file);
  const storage = new Storage(absPath);
  const context = encode(Error().stack);
  const key = "file@" + context + "@" + absPath;
  const lastRun = storage[key];
  const { mtime } = await Deno.stat(absPath);
  storage[key] = new Date();
  return lastRun ? mtime > new Date(lastRun) : true;
}
