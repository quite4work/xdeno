import { exec as kubectlExec, getFirstPod, sshPod } from "./kubectl.js";

async function console(opts = {}) {
  const { context } = opts;
  const { pod } = await getFirstPod("gitlab", { context });
  await sshPod(pod, { cmd: "gitlab-rails console", context });
}

async function exec(opts = {}) {
  const { src, context } = opts;
  const { pod } = await getFirstPod("gitlab", { context });
  await kubectlExec(pod, "gitlab-rails runner -", {
    context,
    stdinFile: src,
    verbosity: 2,
  });
}

export function gitlabYargsCommands(opts) {
  return [
    {
      command: "gitlab exec",
      desc: "Execute script in GitLab rails context",
      builder: { src: { alias: "s", desc: "" } },
      handler: (argv) => exec({ ...opts, ...argv }),
    },
    {
      command: "gitlab console",
      desc: "Run GitLab console",
      builder: {},
      handler: (argv) => console({ ...opts, ...argv }),
    },
  ];
}
