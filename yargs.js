// DEPRECATED

import yargs from "https://deno.land/x/yargs@v17.5.1-deno/deno.ts";
import { groupBy } from "https://deno.land/std@0.148.0/collections/group_by.ts";

export default function (commands = []) {
  return yargs(Deno.args) // Deno.args is still important for Deno.
    .demandCommand().recommendCommands().strict()
    .parserConfiguration({
      // TODO: "halt-at-non-option": true,
      "unknown-options-as-args": true,
      "sort-commands": true,
    })
    .scriptName(basename(Deno.mainModule))
    .usage("$0")
    .help()
    .commands(processCommands(commands));
}

function processCommands(cmds0) {
  const uniqueCmds = {};
  for (const cmd of cmds0) {
    if (cmd.command) {
      uniqueCmds[cmd.command] = cmd;
    }
  }
  const cmds = Object.values(uniqueCmds);
  const groups = groupBy(cmds, ({ command }) => yargs().parse(command)._[0]);
  const res = [];
  for (const [prefix, cmds] of Object.entries(groups)) {
    if (cmds.length === 1) {
      res.push(cmds[0]);
    } else {
      const cmds2 = cmds.filter((cmd) => {
        const { command } = cmd;
        if (command === prefix) {
          res.push(cmd);
        }
        return command !== prefix;
      });
      if (cmds2.length > 0) {
        const cmds3 = cmds2.map((cmd) => {
          const command = cmd.command.replace(new RegExp(`^${prefix}\\s+`), "");
          return { ...cmd, command };
        });
        res.push({
          command: prefix,
          desc: "",
          builder: (yargs) => yargs.commands(cmds3),
        });
      }
    }
  }
  return res;
}
